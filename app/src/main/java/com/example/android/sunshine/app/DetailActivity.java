package com.example.android.sunshine.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class DetailActivity extends ActionBarActivity {
	private static final String LOG_TAG = DetailActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new DetailFragment())
					.commit();
		}
	}



	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class DetailFragment extends Fragment {
		private static final String FORECAST_SHARE_HASHTAG =" #SunshineApp" ;
		private ShareActionProvider mShareActionProvider;
		private String forecast_str;

		public DetailFragment() {
			setHasOptionsMenu(true);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
		                         Bundle savedInstanceState) {

			View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
			Intent intent = getActivity().getIntent();
			 forecast_str = intent.getStringExtra(Intent.EXTRA_TEXT);
			TextView forecastTextView = (TextView) rootView.findViewById(R.id.text);
			forecastTextView.setText(forecast_str);
			return rootView;
		}

		@Override
		public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
			inflater.inflate(R.menu.detail, menu);
			MenuItem item = menu.findItem(R.id.action_share);
			mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
			if (mShareActionProvider != null ) {
				mShareActionProvider.setShareIntent(createShareForecastIntent());
			} else {
				Log.d(LOG_TAG, "Share Action Provider is null???");
			}
		}

		private Intent createShareForecastIntent() {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT,forecast_str+FORECAST_SHARE_HASHTAG);
			return intent;
		}

	}
}